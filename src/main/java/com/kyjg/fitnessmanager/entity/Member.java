package com.kyjg.fitnessmanager.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
public class Member {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false, length = 10)
    private String currentWeight;
    @Column(nullable = false, length = 10)
    private String targetWeight;
    @Column(nullable = false, length = 20)
    private String memberName;
    @Column(nullable = false)
    private Boolean isMan;
    @Column(nullable = false)
    private Date register;
}
