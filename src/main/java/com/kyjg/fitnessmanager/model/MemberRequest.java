package com.kyjg.fitnessmanager.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Getter
@Setter
public class MemberRequest {
    @NotNull
    @Length(min = 1, max = 10)
    private String currentWeight;
    @NotNull
    @Length(min = 1, max = 10)
    private String targetWeight;
    @NotNull
    @Length(min = 1, max = 20)
    private String memberName;
    @NotNull
    private Boolean isMan;
    @NotNull
    private Date register;
}
