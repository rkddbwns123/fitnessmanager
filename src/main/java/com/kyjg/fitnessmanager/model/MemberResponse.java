package com.kyjg.fitnessmanager.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class MemberResponse {
    private Long id;
    private String currentWeight;
    private String targetWeight;
    private String memberName;
    private Boolean isMan;
    private Date register;
}
