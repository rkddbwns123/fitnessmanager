package com.kyjg.fitnessmanager.repository;

import com.kyjg.fitnessmanager.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MemberRepository extends JpaRepository<Member, Long> {
}
