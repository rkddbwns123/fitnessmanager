package com.kyjg.fitnessmanager.controller;

import com.kyjg.fitnessmanager.entity.Member;
import com.kyjg.fitnessmanager.model.MemberItem;
import com.kyjg.fitnessmanager.model.MemberRequest;
import com.kyjg.fitnessmanager.model.MemberResponse;
import com.kyjg.fitnessmanager.service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/member")
public class MemberController {
    private final MemberService memberService;
    @PostMapping("/data")
    public String setMember(@RequestBody @Valid MemberRequest request) {
        memberService.setMember(request.getCurrentWeight(), request.getTargetWeight(), request.getMemberName(),
                request.getIsMan(), request.getRegister());
        return "OK";
    }
    @GetMapping("/all")
    public List<MemberItem> getMembers() {
        List<MemberItem> result = memberService.getMembers();
        return result;
    }
    @GetMapping("/data/id/{id}")
    public MemberResponse getMember(@PathVariable long id) {
        MemberResponse result = memberService.getMember(id);
        return result;
    }
}
