package com.kyjg.fitnessmanager.service;

import com.kyjg.fitnessmanager.entity.Member;
import com.kyjg.fitnessmanager.model.MemberItem;
import com.kyjg.fitnessmanager.model.MemberResponse;
import com.kyjg.fitnessmanager.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;

    public void setMember(String currentWeight, String targetWeight, String memberName, Boolean isMan, Date register) {
        Member addData = new Member();
        addData.setCurrentWeight(currentWeight);
        addData.setTargetWeight(targetWeight);
        addData.setMemberName(memberName);
        addData.setIsMan(isMan);
        addData.setRegister(register);

        memberRepository.save(addData);
    }

    public List<MemberItem> getMembers() {
        List<Member> originList = memberRepository.findAll();

        List<MemberItem> result = new LinkedList<>();

        for (Member item : originList) {
            MemberItem addItem = new MemberItem();
            addItem.setId(item.getId());
            addItem.setCurrentWeight(item.getCurrentWeight());
            addItem.setTargetWeight(item.getTargetWeight());
            addItem.setMemberName(item.getMemberName());
            addItem.setIsMan(item.getIsMan());
            addItem.setRegister(item.getRegister());

            result.add(addItem);

        }

        return result;

    }
    public MemberResponse getMember(long id) {
        Member originData = memberRepository.findById(id).orElseThrow();

        MemberResponse result = new MemberResponse();
        result.setId(originData.getId());
        result.setCurrentWeight(originData.getCurrentWeight());
        result.setTargetWeight(originData.getTargetWeight());
        result.setMemberName(originData.getMemberName());
        result.setIsMan(originData.getIsMan());
        result.setRegister(originData.getRegister());

        return result;
    }
}
